#Geoportal

Web Mapping made easy

## Geoportal

HTML/CSS/JavaScript made portal for web mapping

## Geoportal-generator

Desktop tool to generate a custom version of Geoportal

## Geoportal-tags

Web app for geotagging based on Geoportal
